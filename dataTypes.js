const integer=10;
const float=9.99;
const double=9.00;
const negative=-20;

console.log(typeof integer);
const doubleQoutedString="hii"
const singleQuotedString='bye'
const backTickedString="ticks found on keyboard";
//collection datatypes
//1.array
//2.object

//arrays are comma seperated elements of any data types enclosed in square brackets []
var numbers=new Array(1,2,3,4);
const numbers1=[1,2,3,4,5,6];
const names=["abcd","efgf"];
const booleans=[true,false,true,false];
const matrix=[[1,2,3],[4,5,6]];
const assortedArray=[
    10,"this is an array",false,undefined,null,[1,23,89],
    {
        name:"pradnya"
    },
    function (){console.log("hey")}
];

//objects
//key:value seperated by commas
//enclosed in {}
// array is printed as object when checked with typeOf
const person={
    name:"pradnya",
    age:22,
    technology:['html','css','js','spring'],
    city:"pune",
    address:{
        country:"india",
        state:"maharashtra"
    },
    speak:function(){console.log("nodejs is great")}
}

//function 
function learnNode(){
    console.log("mug up the methods");
}


