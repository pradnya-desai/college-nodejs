//non parameterized function, non returning function
function greetUser(){
console.log("hello");
}

greetUser(); //invokes the function
//reference function/anonymous/variable function
//function keyword
//fat arrow function


function greetUserName(username){
    console.log(`hello,${username}`);
}

greetUserName("pradnya");


//parameterized return function
function add(num1,num2){
    return num1+num2;

}
const num3=add(5,7);
console.log(num3);
const concatenation=add("this is the first half", "this is the second half");
console.log(concatenation);

//reference function/anonymous function/ function expression
const anonymous=function(){

}
anonymous();
const difference=subtract(10,3);
console.log(difference);

//fat arrow function
const multiply=(num1,num2)=>{
    return num1*num2;
}

const product=multiply(7,9);
console.log(product);

const divide=(num1,num2)=>num1/num2;

const parent=(fn)=>{
    fn();// fn is a reference of a function
    //we are calling fn and invokimng the function at that reference
//callback references a function
//we can invoke the function at that reference using ();
}
parent(()=>{
    console.log("hello");
})
parent(()=>console.log("called from parent"));
const printSomething=()=>console.log("something");
parent(printSomething);

