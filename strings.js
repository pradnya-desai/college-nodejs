const greeting="hello"
const gname="pradnya"

//merging 2strings/concatenation
//old way
let fullGreeting=greeting + gname; //hellopradnya
fullGreeting=greeting + ' ' + gname; //hello pradnya

//ES6+ way(new way)
//ECMAScript 6
fullGreeting=`${greeting} ${gname}`;
//f"{greeting} {name}"
//@r"{greeting} {name}


//properties
const sentence="this is a sentence";
const length=sentence.length;
console.log(length)

//methods

const capitalizedString=sentence.toUpperCase();
console.log(capitalizedString);

const lowerizedString=sentence.toLowerCase();
console.log(lowerizedString);

//medium priority methods of strings
//substring
const word= "strinng";
//indexes=012345
const char=word[3];
const subString=word.substring(1,5);
console.log(subString);

//indexOf
const index=word.indexOf("n");

//high priority methods
const friends="ross,rachael,joey,monica,chandler,phoebe";
const friendsArray=friends.split(",");
console.log(friendsArray);

//string immutable
let ageString="i am 20";
ageString[5];

ageString="i am 22";
ageString[5];

