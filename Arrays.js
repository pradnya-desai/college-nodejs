const numbers=[1,2,3,4,5];

console.log(numbers[3]);
console.log(numbers[7]);

//property
const len=numbers.length;
console.log(len);//5

numbers[4]=19; //changes the value

//methods
//insert elements 

//insert from the end
numbers.push(6); //inserts elements at last

//insert from the front 
numbers.unshift(7)

//inserting in the middle
numbers.splice(4,0,8);// 4=index where we want to enter element
//0= no of element we want to delete from index 4
//8=the element we want to add at index 4
console.log(numbers);

numbers.pop();//removes the last element and returns it

//remove from the front
numbers.shift(); //removes the first element and returns it

//remove from the middle
numbers.splice(4,1);


console.log(splice);

//iterate, modify, filter
//for each
//iterates over each element in the array
//and calls the callback fn for each element
numbers.forEach((element) => console.log(element));

//modify-->map
//modify that element
//stores modification in new array
//returns new array
const squares=numbers.map((element)=> element **2);
numbers.fi
console.log(squares);

//filter
//callbacks as predicates (that callbacks which return booleans)
const divisibleby2=numbers.filter((element)=>element %2===0)
console.log(divisibleby2);

//destructuring
//es6+

const person=[27,"anniruddha", true];
//const age=person[0];
//const name=person[1];
//const isMarried=person[2];

const [age,name,isMarried]=person;
console.log(age,name,isMarried)

//rest operator
const[age,...restOfThePerson]=person;
console.log(age,restOfThePerson);


//...=>spread operator
const personClone=person;
//copy the reference of the person array into personClone
//personeClone and person point to the same array

personClone.push("pushed this string in person clone")
console.log(person);

const personClone1=[...person];

//create new array and copies/spreads out all the elements from person in that array
personClone1.push("pushed this string in personclone1")




const primes=[2,3,5,7,11,13];
//1. index at which we want to perform add/remove
//2nd parameter no of elements we want to delete
//3rd parameter elements we want to add to the array

primes.splice(2,0,15,19,23,27)
console.log(primes);


primes.splice(2,4);
console.log(primes);

