//if else if else
//switch

// if(<condition>){
// executes this block
// }
const heightThreshold=3.5;
const personHeight=5;

if(personHeight>=heightThreshold){
    console.log("allowed");
}
//else is not compulsory
//else block is not standalone
//elseif block is not standalone

else
{
console.log("not allowed");
}

const age=20;
const minimumAge=25;
if(age>=minimumAge)
{console.log("allowed")}
else if(age>20)
{console.log("partially allowed");}
else
{console.log("not allowed")}


const option = "pikachu"
switch (option) {
    case "pikachu":
        console.log("electric pokemon");
        break;
    case "charizard":
        console.log("fire pokemon");
        break;
    case "onyx":
        console.log("normal pokemon");
        break;
    default:
        console.log("bye");

}

