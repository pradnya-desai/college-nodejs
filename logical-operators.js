//combine two booleans for 1 boolean
//OR--> ||
//OR tends towards true
//evaluates to false when both booleans are false
true|| true //true
true|| false //true
false|| true //true
false|| false //false

//AND --> &&
//AND tends towards false
//evalutes to true when both booleans are true
true   &&  true //true
true   &&  false //false
false  &&  true //false
false  &&  false //false

//FALSY & TrUTHY VALUES

//"",0,null,undefined,NaN,false

1||""//true
0|| "abcd" //true

1 && ""//false
0 && "abcd" //false