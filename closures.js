const outerFunction=()=>{
    // let num1=10;
    // let counter=0;
    // for (counter=0;counter<10;counter++){}
    // counter=counter+1;
    // num1=num1+5;
    // console.log(num1,counter);
    let counter=0
    const inner=()=>{
        counter=counter+1;
        return counter;
    }
    //reference of inner function is returned
    //Memory Leak=>used as a utility
    return inner;
}
//reference of the inner fn
const returnOuterFn=outerFunction(); //execution context 1
 
const counter=outerFunction(); //execution context 2

console.log(returnOuterFn());
console.log(counter());
//outerFunction  does not create closure