//compare LHS to RHS
//evaluate to booleans(true/false)

//==,!=,>,>=,<,<=

5==5//true
5==6//false
4==5//false
5== "5" //value is compared even if a value is string -no datatype checking- returns true
5== "five" //false coz values are different
"a"=="a" //true
"a"=="b"// false





5!=5//false
5!=6//true
4!=5//true
5!= "5" //false
5!= "five" //false
"a"!="b"//true

5>5//false
5>6//false
4>5//false
5>4//true
5>"5"//false
5>"five"//false
"a">"a"//false
"a">"b"//false
"b">"a"//true

//common pitfalls

[]==[];//false //memory location of arrays are compared for equality
[]===[];//false
{}=={};//false
{}==={};//false

//arrays or objects are going to be checked with references and not values
//===